import pytest
from models import app, db, Destination, PointsOfInterest, Hotel
import json

# Load JSON data for testing
def load_json(filename):
    with open(filename) as file:
        data = json.load(file)
    return data

destinations_data = load_json('destinations.json')
points_of_interest_data = load_json('pointsofinterest.json')
hotels_data = load_json('hotels.json')

@pytest.fixture(scope='module')
def test_client():
    flask_app = app
    flask_app.config['TESTING'] = True
    flask_app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///:memory:'  # In-memory SQLite for testing
    flask_app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

    testing_client = flask_app.test_client()

    ctx = flask_app.app_context()
    ctx.push()

    db.create_all()

    yield testing_client  # this is where the testing happens!

    db.session.remove()
    db.drop_all()
    ctx.pop()

@pytest.fixture(scope='module')
def init_database():
    # Add destinations
    for city_info in destinations_data['destinations']:
        destination = Destination(cityName=city_info["cityName"], location=city_info["location"], latitude=city_info["latitude"], longitude=city_info["longitude"], website=city_info["website"], photo_url=city_info["photo_url"])
        db.session.add(destination)
    
    # Add points of interest
    for poi in points_of_interest_data['point_of_interest']:
        point_of_interest = PointsOfInterest(name=poi["name"], vicinity=poi["vicinity"], rating=poi["rating"], total_ratings=poi["total_ratings"], photo_url=poi["photo_url"])
        point_of_interest.cityName = poi["cityName"]
        db.session.add(point_of_interest)

    # Add hotels
    for hotel in hotels_data['hotels']:
        hotel_instance = Hotel(name=hotel["name"], vicinity=hotel["vicinity"], rating=hotel["rating"], total_ratings=hotel["total_ratings"], photo_url=hotel["photo_url"])
        hotel_instance.cityName = hotel["cityName"]
        db.session.add(hotel_instance)

    db.session.commit()

@pytest.mark.usefixtures("init_database")
class TestModels:

    def test_get_all_destinations(self, test_client):
        response = test_client.get('/destinations')
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert len(data['destinations']) == len(destinations_data['destinations'])

    def test_get_destination(self, test_client):
        city_name = destinations_data['destinations'][0]['cityName']
        response = test_client.get(f'/destinations/{city_name}')
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert data['destinations']['cityName'] == city_name

    def test_create_destination(self, test_client):
        new_city = {
            "cityName": "Test City",
            "location": "Test Location",
            "latitude": 12.34,
            "longitude": 56.78,
            "website": "http://testcity.com",
            "photo_url": "http://testcity.com/photo.jpg"
        }
        response = test_client.post('/destinations', json=new_city)
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert data['cityName'] == new_city['cityName']

    def test_get_all_points_of_interest(self, test_client):
        response = test_client.get('/pointsofinterest')
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert len(data['point_of_interest']) == len(points_of_interest_data['point_of_interest'])

    def test_get_point_of_interest(self, test_client):
        id = points_of_interest_data['point_of_interest'][0]['id']
        response = test_client.get(f'/pointofinterest/{id}')
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert len(data['point_of_interest']) > 0

    def test_create_point_of_interest(self, test_client):
        new_poi = {
            "name": "Test POI",
            "vicinity": "Test Vicinity",
            "rating": 4.5,
            "total_ratings": 100,
            "photo_url": "http://testpoi.com/photo.jpg",
            "cityName": destinations_data['destinations'][0]['cityName']
        }
        response = test_client.post('/pointsofinterest', json=new_poi)
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert len(data['point_of_intersts']) > 0

    def test_get_all_hotels(self, test_client):
        response = test_client.get('/hotels')
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert len(data['hotels']) == len(hotels_data['hotels'])

    def test_get_hotel(self, test_client):
        id = hotels_data['hotels'][0]['id']
        response = test_client.get(f'/hotels/{id}')
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert len(data['hotels']) > 0

    def test_create_hotel(self, test_client):
        new_hotel = {
            "name": "Test Hotel",
            "vicinity": "Test Vicinity",
            "rating": 4.5,
            "total_ratings": 100,
            "photo_url": "http://testhotel.com/photo.jpg",
            "cityName": destinations_data['destinations'][0]['cityName']
        }
        response = test_client.post('/hotels', json=new_hotel)
        assert response.status_code == 200
        data = json.loads(response.data.decode())
        assert len(data['hotels']) > 0
