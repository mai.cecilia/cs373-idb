import googlemaps
from datetime import datetime
import requests

API_KEY = 'AIzaSyCpWSmKrN5gQAmZm0BR7E32q9fCRILWOWo'
PLACES_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json"
PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json"

def get_city_information(cityName):
        cityDetails = []
        cityParams = {
            'inputtype': 'textquery',
            'key': API_KEY,
        }
        cityDetailParams = {
            'key': API_KEY,
        }
        cityParams['input'] = cityName
        city = requests.get(PLACES_SEARCH_URL, cityParams, timeout=600).json()['candidates']
        print("******  cityName,", cityName)
        thisCity = city[0]['place_id']
        cityDetailParams['placeid'] = thisCity
        results = requests.get(PLACES_DETAILS_URL, cityDetailParams).json().get('result')
        website = ""
        if 'website' in results:
            website = results['website']
        location = results['formatted_address']
        lat = results['geometry']['location']['lat']
        long = results['geometry']['location']['lng']
        photo_url = None
        if 'photos' in results:
            photo_reference = results['photos'][0]['photo_reference']
            photo_url = f"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_reference}&key={API_KEY}"
        cityDetails = {'name': cityName, 'Latitude': lat, 'Longitude': long, 'Address': location, 'Web_site': website, 'photo_url': photo_url}
        return cityDetails

def get_city_information2(city_name):
    gmaps = googlemaps.Client(key=API_KEY)
    geocode_result = gmaps.geocode(city_name)
    if geocode_result:
        location = geocode_result[0]['geometry']['location']
        lat = location['lat']
        lng = location['lng']

        formatted_address = geocode_result[0]['formatted_address']

        place_details = gmaps.place(geocode_result[0]['place_id'])

        if place_details:

            photo_reference = place_details['result'].get('photos', [])[0].get('photo_reference', None)
            if photo_reference:
                photo_url = get_photo_url(photo_reference)
            else:
                print("No photos found for this place.")
                
            city_info = {'name': city_name, 'Latitude': lat, 'Longitude': lng, 'Address': formatted_address, 'Web_site': "To be done", 'photo_url': photo_url}
            return city_info

    else:
        print(f"No geocode results found for {city_name}")
        return None

def get_photo_url(photo_reference):
    base_url = 'https://maps.googleapis.com/maps/api/place/photo'
    max_width = 400  
    params = {
        'photoreference': photo_reference,
        'maxwidth': max_width,
        'key': API_KEY
    }
    response = requests.get(base_url, params=params, timeout=600)
    
    if response.status_code == 200:
        return response.url
    else:
        print(f"Failed to fetch photo. Status code: {response.status_code}")
        return None

if __name__ == '__main__':
    WIKIPEDIA_API_URL = "https://en.wikipedia.org/w/api.php?action=query&prop=links&titles=List_of_cities_by_international_visitors&format=json"

    try:
        city_info = []
        destinations = requests.get(f"{WIKIPEDIA_API_URL}", timeout=600).json()
        print(destinations, "\n\n")
        city_list = [
            destinations["query"]["pages"]["58606989"]["links"][0]["title"],
            destinations["query"]["pages"]["58606989"]["links"][1]["title"],
            destinations["query"]["pages"]["58606989"]["links"][2]["title"],
        ]
        for city_name in city_list:
            city_info.append(get_city_information(city_name))

        print(city_info)
        # return render_template('public/destinations.html', cities=city_info)
    except Exception as e:
        print("exception:", e)
        