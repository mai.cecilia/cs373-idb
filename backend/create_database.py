from models import app, db, Destination, PointsOfInterest, Hotel
from backend_utilities import find_hotel, find_points_of_interest
from city import get_city_information, get_photo_url

def init_db(city_list):  
    for city_name in city_list:
        destination = Destination.query.filter_by(cityName=city_name)
        city_info = get_city_information(city_name)
        destination = Destination(city_info["name"], city_info["Address"], city_info["Latitude"], city_info["Longitude"], city_info["Web_site"], city_info["photo_url"])
        db.session.add(destination)
        db.session.commit()
        
        point_of_interests = PointsOfInterest.query.filter_by(cityName=city_name).all()
        if len(point_of_interests) == 0:
            poi_list = find_points_of_interest(city_name)
            for poi in poi_list:
                point_of_interest = PointsOfInterest(poi["name"], poi["vicinity"], poi["rating"], poi["user_ratings_total"], poi["photo_url"])
                destination.points_of_interests.append(point_of_interest)
                db.session.add(point_of_interest)
                db.session.commit()
        
        hotels = Hotel.query.filter_by(cityName=city_name).all()
        if len(hotels) == 0:
            hotel_list = find_hotel(city_name)
            for hotel in hotel_list:
                hotel = Hotel(hotel["name"], hotel["vicinity"], hotel["rating"], hotel["user_ratings_total"], hotel["photo_url"])
                destination.hotels.append(hotel)
                hotel.destination = destination
                db.session.add(hotel)
                db.session.commit()

db.drop_all()
db.create_all()

# city_list = ["Austin", "Dallas", "Houston", "New York", "Istanbul", "Rome", "Tokyo", "London", "Dubai", "Los Angeles"]

city_list = ["Austin", "Dallas", "Houston", "New York", "Istanbul", "Rome", "Tokyo", "London", "Dubai", "Los Angeles",
             "Las Vegas", "Singapore", "Bangkok", "Barcelona", "Berlin", "Hong Kong", "Prague", "San Francisco", "Seoul", "Chicago",
             "Amsterdam", "Dublin", "Kuala Lumpur", "Madrid", "Osaka", "Beijing", "Vienna", "Lisbon", "Shanghai", "Melbourne", "Sdyney",
             "Orlando", "Frankfurt", "Kyoto", "Taipei", "Toronto", "Florence", "Zurich", "Munich", "Miami", "Venice", "Stockholm",
             "Cancun", "Abu Dhabi", "Brussels", "Tel Avivi", "Warsaw", "Guangzhou", "Copenhagan", "Budapest", "Vancouver", "Mexico City",
             "Antalya", "Atlanta", "Busan", "Rio de Janeiro", "Edinburgh", "Montreal", "Macau", "Doha", "Buenos Aires", "Lima",
             "Delhi", "Phuket", "Tallinin", "Ho Chi Minh City", "Hanoi", "Cairo", "Guilin", "Mecca", "Tbilisi", "Playa del Carmen",
             "Maui", "Philadephia", "San Diego", "Tucson", "Seattle", "San Antonio", "Jacksonville", "Fort Worth", "Charlotte",
             "Portland", "Memphis", "Baltimore", "Milwaukee", "Fresno", "Detroit", "Sacramento", "Omaha", "Kansas City", "Colorado Springs", 
             "Boulder", "Raleigh", "Tampa", "Long Beach", "Minneapolis", "New Orleans", "Cleveland", "Honolulu", "Stockton",
             "Galveston", "Pittsburgh", "St. Luois", "Anchorage", "Savannah", "Denver", "Oklahoma City", "Nashville", "Charleston", "Santa Fe",
             "Santa Clara", "Sedona", "San Jose"]

init_db(city_list)