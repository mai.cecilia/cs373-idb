import requests

def find_hotel(city_name, radius=5000):
    api_key = 'AIzaSyCpWSmKrN5gQAmZm0BR7E32q9fCRILWOWo'

    hotel_endpoint = "https://maps.googleapis.com/maps/api/geocode/json"
    hotel_params = {
        'address': city_name,
        'key': api_key
    }
    
    geocoding_response = requests.get(hotel_endpoint, params=hotel_params, timeout=600)
    geocoding_results = geocoding_response.json().get('results')
    
    if geocoding_results:
        location = geocoding_results[0]['geometry']['location']
        lat = location['lat']
        lng = location['lng']
        
        location = f"{lat},{lng}"
        keyword='hotel'
        endpoint_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
        
        params = {
            'location': location,
            'radius': radius,
            'keyword': keyword,
            'key': api_key
        }
        
        response = requests.get(endpoint_url, params=params, verify=False, timeout=600)
        results = response.json().get('results')
        

        if results:
            hotel_list = []
            count = 0
            for place in results:
                if count >= 2:
                    break
                name = place['name']
                vicinity = place['vicinity']
                rating = place.get('rating', 'Not available')
                user_ratings = place.get('user_ratings_total', 'No ratings')
                if 'photos' in place:
                    photo_reference = place['photos'][0]['photo_reference']
                    photo_url = f"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_reference}&key={api_key}"
                else:
                    photo_url = None
                count += 1
                hotel_list.append({'name': name, 'vicinity': vicinity, 'rating': rating, 'user_ratings_total': user_ratings, 'photo_url': photo_url})
            # return render_template('hotel.html', hotel_list=hotel_list)
            return hotel_list
        else:
            print("No hotels found.")
            return None
    else:
        print(f"Could not find coordinates for {city_name}")


def find_points_of_interest(city_name, radius=5000, types='tourist_attraction'):
    api_key = 'AIzaSyCpWSmKrN5gQAmZm0BR7E32q9fCRILWOWo'
    
    geocoding_endpoint = "https://maps.googleapis.com/maps/api/geocode/json"
    geocoding_params = {
        'address': city_name,
        'key': api_key
    }
    
    geocoding_response = requests.get(geocoding_endpoint, params=geocoding_params, timeout=600)
    geocoding_results = geocoding_response.json().get('results')
    
    if geocoding_results:
        location = geocoding_results[0]['geometry']['location']
        lat = location['lat']
        lng = location['lng']
        
        location_str = f"{lat},{lng}"
        endpoint_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"
    
        params = {
            'location': location_str,
            'radius': radius,
            'types': types,
            'key': api_key
        }
        
        response = requests.get(endpoint_url, params=params, timeout=600)
        results = response.json().get('results')
        
        poi_list = []
        if results:
            count = 0
            for place in results:
                if count >= 2:
                    break
                name = place['name']
                vicinity = place['vicinity']
                rating = place.get('rating', 'Not available')
                user_ratings = place.get('user_ratings_total', 'No ratings')
                if 'photos' in place:
                    photo_reference = place['photos'][0]['photo_reference']
                    photo_url = f"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_reference}&key={api_key}"
                else:
                    photo_url = None
                count += 1
                poi_list.append({'name': name, 'vicinity': vicinity, 'rating': rating, 'user_ratings_total': user_ratings, 'photo_url': photo_url})
            # return render_template('points_of_interest.html', points_of_interest=poi_list)
        
            return poi_list
        else:
            print("No points of interest found for ", city_name)
            return None
