from flask import Flask, render_template, request, jsonify
from flask_sqlalchemy import SQLAlchemy
from sqlalchemy.orm.exc import NoResultFound
from flask_cors import CORS
from backend_utilities import find_hotel, find_points_of_interest
from city import get_city_information, get_photo_url
import json
import requests

app = Flask(__name__)
CORS(app)
app.app_context().push()


# API FOR ABOUT
GITLAB_API_URL = "https://gitlab.com/api/v4/projects/59138519"


# Team Members
TEAM_MEMBERS = {
    "Ayon Das": {"emails": ["ayon.s.das@hotmail.com"], "username": "ayonsdas", "role": "Backend"},
    "Cecilia Mai": {"emails": ["mai.cecilia@utexas.edu"], "username": "mai.cecilia", "role": "Frontend"},
    "Eli Zunker": {"emails": ["epzunker@gmail.com"], "username": "ezunker", "role": "Frontend"},
    "Jeffrey Chen": {"emails": ["jeffreychen287@gmail.com"], "username": "jeffreychen287", "role": "Backend"},
    "Nathaniel Ruiz": {"emails": ["ruizn9985@gmail.com", "ruizn9985@utexas.edu"], "username": "Nathan9985", "role": "Frontend"}
}

def fetch_all(extension):
    url = f"{GITLAB_API_URL}/{extension}"
    params = {'per_page': 100, 'page': 1}
    all_data = []

    while True:
        response = requests.get(url, params=params)
        data = response.json()
        if not data:
            break
        all_data.extend(data)
        params['page'] += 1

    return all_data

def aggregate_data():
    commits = fetch_all("repository/commits")
    branches = fetch_all("repository/branches")
    issues = fetch_all("issues")
    contributors = fetch_all("repository/contributors")

    for member in TEAM_MEMBERS.values():
        member["commits"] = len([commit for commit in commits if commit["author_email"] in member["emails"]])
        member["issues_closed"] = len([issue for issue in issues if issue["closed_by"] and issue["closed_by"]["username"] == member["username"]])
        member["issues"] = len([issue for issue in issues if issue["author"]["username"] == member["username"]])

    stats = {
        "num_commits": len(commits),
        "num_branches": len(branches),
        "num_contributors": len(contributors),
        "total_issues": len(issues),
        "team_members": TEAM_MEMBERS
    }

    return stats

app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql+psycopg2://postgres:jyc752@/postgres?host=/cloudsql/cs373-idb-group4-summer2024:us-central1:tripsmartdb/'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True 
db = SQLAlchemy(app)


class Destination(db.Model):
    __tablename__ = 'destination'
    # id = db.Column(db.Integer, primary_key=True)
    cityName = db.Column(db.String, primary_key=True)
    location = db.Column(db.String, nullable=False)
    latitude = db.Column(db.Float, nullable=False)
    longitude = db.Column(db.Float, nullable=False)
    website = db.Column(db.String, nullable=False)
    photo_url = db.Column(db.String, nullable=False)

    points_of_interests = db.relationship('PointsOfInterest', backref='destination')
    hotels = db.relationship('Hotel', backref = 'destination')
    
    def __init__(self, cityName, location, latitude, longitude, website, photo_url):
        self.cityName = cityName
        self.location = location
        self.latitude = latitude
        self.longitude = longitude
        self.website = website
        self.photo_url = photo_url
        
class PointsOfInterest(db.Model):
    __tablename__ = 'pointsofinterests'
    id = db.Column(db.Integer, primary_key=True)
    cityName = db.Column(db.String, db.ForeignKey('destination.cityName'))
    name = db.Column(db.String, nullable=False)
    vicinity = db.Column(db.String, nullable=False)
    photo_url = db.Column(db.String, nullable=False)
    rating = db.Column(db.Float, nullable=False)
    total_ratings = db.Column(db.Integer, nullable=False)

    def __init__(self, name, vicinity, rating, total_ratings, photo_url):
        self.name = name
        self.vicinity = vicinity
        self.photo_url = photo_url
        self.rating = rating
        self.total_ratings = total_ratings

class Hotel(db.Model):
    __tablename__ = 'hotels'

    id = db.Column(db.Integer, primary_key=True)
    cityName = db.Column(db.String, db.ForeignKey('destination.cityName'))
    name = db.Column(db.String, nullable=False)
    vicinity = db.Column(db.String, nullable=False)
    photo_url = db.Column(db.String, nullable=False)
    rating = db.Column(db.Float, nullable=False)
    total_ratings = db.Column(db.Integer, nullable=False)
    
    def __init__(self, name, vicinity, rating, total_ratings, photo_url):
        self.name = name
        self.vicinity = vicinity
        self.photo_url = photo_url
        self.rating = rating
        self.total_ratings = total_ratings
        

def format_destination_basic(destination):
    return {
        "cityName": destination.cityName,
        "location": destination.location,
        "latitude": destination.latitude,
        "longitude": destination.longitude,
        "website": destination.website,
        "photo_url": destination.photo_url,
    }

def format_point_of_interest_basic(poi):
    return {
        "name": poi.name,
        "id": poi.id,
        "cityName": poi.cityName,
        "vicinity": poi.vicinity,
        "rating": poi.rating,
        "total_ratings": poi.total_ratings,
        "photo_url": poi.photo_url    
    }

def format_hotel_basic(hotel):
    return {
        "name": hotel.name,
        "id": hotel.id,
        "cityName": hotel.cityName,
        "vicinity": hotel.vicinity,
        "rating": hotel.rating,
        "total_ratings": hotel.total_ratings,
        "photo_url": hotel.photo_url    
    }


def format_destination(destination):
    formatted_destination = format_destination_basic(destination)
    formatted_destination["points_of_interest"] = [format_point_of_interest_basic(poi) for poi in destination.points_of_interests]
    formatted_destination["hotels"] = [format_hotel_basic(hotel) for hotel in destination.hotels]
    return formatted_destination

def format_point_of_interest(poi):
    formatted_poi = format_point_of_interest_basic(poi)
    destination = Destination.query.filter_by(cityName=poi.cityName).first()
    formatted_poi["hotels"] = [format_hotel_basic(hotel) for hotel in destination.hotels if hotel.cityName == poi.cityName]
    return formatted_poi

def format_hotel(hotel):
    formatted_hotel = format_hotel_basic(hotel)
    destination = Destination.query.filter_by(cityName=hotel.cityName).first()
    formatted_hotel["points_of_interest"] = [format_point_of_interest_basic(poi) for poi in destination.points_of_interests if hotel.cityName ==  poi.cityName]
    return formatted_hotel


@app.route('/')
def hello():
    return "Hey!"

@app.route('/about')
def about():
    try:
        stats = aggregate_data()
        return stats
    except Exception as e:
        return { "result": "Not found" }
        # return render_template('public/error.html', error=str(e))

@app.route('/destinations', methods=['POST'])
def create_destination():
    cityName = request.json['cityName']
    destination = Destination.query.filter_by(cityName=cityName).first()
    if destination is None:
        city_info = get_city_information(cityName)
        destination = Destination(city_info["name"], city_info["Address"], city_info["Latitude"], city_info["Longitude"], city_info["Web_site"], city_info["photo_url"])
        db.session.add(destination)
        db.session.commit()
    return format_destination(destination)
    
@app.route('/destinations', methods=['GET'])
def get_all_destinations():
    destinations = Destination.query.order_by(Destination.cityName.asc()).all()
    destination_list = []
    for destination in destinations:
        destination_list.append(format_destination(destination))
    return {'destinations': destination_list}
        
@app.route('/destinations/<cityName>', methods=['GET'])
def get_destinations(cityName):
    destination = Destination.query.filter_by(cityName=cityName).one()
    formatted_destination = format_destination(destination)
    return formatted_destination

@app.route('/destinations/<cityName>', methods=['DELETE'])
def delete_destination(cityName):
    destination = Destination.query.filter_by(cityName=cityName).one()
    db.session.delete(destination)
    db.session.commit()
    return f'destination city: {cityName} is deleted'
        
@app.route('/pointsofinterest', methods=['POST'])
def create_point_of_interest():
    cityName = request.json['cityName']
    point_of_interests = PointsOfInterest.query.filter_by(cityName=cityName).all()
    if len(point_of_interests) > 0:
        poi_list = []
        for point_of_interest in point_of_interests:
            poi_list.append(format_point_of_interest(point_of_interest))
    else:
        destination = Destination.query.filter_by(cityName=cityName).first()
        if destination is None:
            city_info = get_city_information(cityName)
            destination = Destination(city_info["name"], city_info["Address"], city_info["Latitude"], city_info["Longitude"], city_info["Web_site"], city_info["photo_url"])
            db.session.add(destination)

        poi_list = find_points_of_interest(cityName)
        for poi in poi_list:
            point_of_interest = PointsOfInterest(poi["name"], poi["vicinity"], poi["rating"], poi["user_ratings_total"], poi["photo_url"])
            point_of_interest.destination = destination
            db.session.add(point_of_interest)
            db.session.commit()
        
    return {'point_of_intersts': poi_list}

@app.route('/pointsofinterest', methods=['GET'])
def get_all_point_of_interest():
    point_of_interests = PointsOfInterest.query.order_by(PointsOfInterest.cityName.asc()).all()
    point_of_interest_list = []
    for point_of_interest in point_of_interests:
        point_of_interest_list.append(format_point_of_interest(point_of_interest))
    return {'point_of_interest': point_of_interest_list}

@app.route('/pointsofinterest/<id>', methods=['GET'])
def get_point_of_interest(id):
    point_of_interest = PointsOfInterest.query.filter_by(id=id).first()
    return format_point_of_interest(point_of_interest)
 
@app.route('/pointsofinterest/<cityName>', methods=['DELETE'])
def delete_point_of_interest(cityName):
    point_of_interests = PointsOfInterest.query.filter_by(cityName=cityName).all()
    for point_of_interest in point_of_interests:
        db.session.delete(point_of_interest)
        db.session.commit()
    return f'point_of_interest for city {cityName} is deleted'

@app.route('/hotels', methods=['POST'])
def create_hotel():
    cityName = request.json['cityName']
    hotels = Hotel.query.filter_by(cityName=cityName).all()
    if len(hotels) == 0:
        destination = Destination.query.filter_by(cityName=cityName).first()
        if destination is None:
            city_info = get_city_information(cityName)
            destination = Destination(city_info["name"], city_info["Address"], city_info["Latitude"], city_info["Longitude"], city_info["Web_site"], city_info["photo_url"])
            db.session.add(destination)

        hotel_list = find_hotel(cityName)
        for hotel in hotel_list:
            hotel = Hotel(hotel["name"], hotel["vicinity"], hotel["rating"], hotel["user_ratings_total"], hotel["photo_url"])
            hotel.destination = destination
            db.session.add(hotel)
            db.session.commit()
    else:
        hotel_list = []
        for hotel in hotels:
            hotel_list.append(format_hotel(hotel))
    return {'hotels': hotel_list}

@app.route('/hotels', methods=['GET'])
def get_all_hotel():
    hotels = Hotel.query.order_by(Hotel.cityName.asc()).all()
    hotel_list = []
    for hotel in hotels:
        hotel_list.append(format_hotel(hotel))
    return {'hotels': hotel_list}

@app.route('/hotels/<id>', methods=['GET'])
def get_hotel(id):
    hotel = Hotel.query.filter_by(id=id).first()
    return format_hotel(hotel)


@app.route('/hotels/<cityName>', methods=['DELETE'])
def delete_hotel(cityName):
    hotels = Hotel.query.filter_by(cityName=cityName).all()
    if len(hotels) > 0:
        for hotel in hotels:
            db.session.delete(hotel)
            db.session.commit()
        return f'hotels for city: {cityName} is deleted'
    else:
        return f'hotels for city: {cityName} not exist'

# API ROUTES BELOW
@app.route('/api')
def api():
    apiMessage = "Welcome to the TripSmart API!"
    return jsonify({"text": apiMessage})

@app.route('/api/destinations', methods=['GET'])
def api_destinations():
    page_term = int(request.args.get('page', 1))
    per_page_term = int(request.args.get('per_page', 10))
    sort_order_term = request.args.get('sort_order', 'asc')

    query = Destination.query
    if sort_order_term == 'asc':
        query = query.order_by(Destination.cityName.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
    else:
        query = query.order_by(Destination.cityName.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)

    try:
        destination_list = []
        for destination in query:
            destination_list.append(format_destination(destination))
        if not destination_list:
            return jsonify({"error": "No results found with given parameters"}), 404
        return jsonify(destination_list)
    except NoResultFound:
        return jsonify({"error": "No results found with given parameters"}), 404

@app.route('/api/destinations/<string:city_name>', methods=['GET'])
def api_destinations_id(city_name):
    destination = Destination.query.get({"cityName": city_name})
    if not destination:
        return jsonify({"error": "Destination not found"}), 404

    pois = PointsOfInterest.query.filter(PointsOfInterest.cityName == city_name).all()
    pois_count = len(pois)
    poi_rating_total = 0
    poi_rating_number = 0
    for poi in pois:
        poi_rating_number += 1
        poi_rating_total += poi.rating

    pois_average_rating = poi_rating_total / poi_rating_number

    hotels = Hotel.query.filter(Hotel.cityName == city_name).all()
    hotels_count = len(hotels)
    hotel_rating_total = 0
    hotel_rating_number = 0
    for hotel in hotels:
        hotel_rating_number += 1
        hotel_rating_total += hotel.rating

    hotels_average_rating = hotel_rating_total / hotel_rating_number

    destination_data = {
        'name': destination.cityName,
        'num_POIs': pois_count,
        'average_POI_rating': pois_average_rating,
        'num_hotels': hotels_count,
        'average_hotel_rating': hotels_average_rating
    }

    return jsonify(destination_data)

# api/hotels
# params: none
# api page to retrieve all hotel instances fitting given query parameters
@app.route('/api/hotels', methods=['GET'])
def api_hotels():
    page_term = int(request.args.get('page', 1))
    per_page_term = int(request.args.get('per_page', 10))
    sort_by_term = request.args.get('sort_by', 'name')
    sort_order_term = request.args.get('sort_order', 'asc')
    city_term = request.args.get('city', 'all')
    min_rating_term = float(request.args.get('min_rating', 0))

    query = Hotel.query
    if sort_by_term == "name":
        if sort_order_term == "asc":
            if city_term == "all":
                query = query.filter(Hotel.rating >= min_rating_term).order_by(Hotel.cityName.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(Hotel.cityName == city_term and Hotel.rating >= min_rating_term).order_by(Hotel.cityName.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
        else:
            if city_term == "all":
                query = query.filter(Hotel.rating >= min_rating_term).order_by(Hotel.cityName.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(Hotel.cityName == city_term and Hotel.rating >= min_rating_term).order_by(Hotel.cityName.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
    else:
        if sort_order_term == "asc":
            if city_term == "all":
                query = query.filter(Hotel.rating >= min_rating_term).order_by(Hotel.rating.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(Hotel.cityName == city_term and Hotel.rating >= min_rating_term).order_by(Hotel.rating.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
        else:
            if city_term == "all":
                query = query.filter(Hotel.rating >= min_rating_term).order_by(Hotel.rating.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(Hotel.cityName == city_term and Hotel.rating >= min_rating_term).order_by(Hotel.rating.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)

    try:
        hotels_list = []
        for hotel in query:
            hotels_list.append(format_hotel_basic(hotel))
        if not hotels_list:
            return jsonify({"error": "No results found with given parameters"}), 404
        return jsonify(hotels_list)
    except NoResultFound:
        return jsonify({"error": "No results found with given parameters"}), 404

# api/hotels/hotel_id
# params: hotel_id
# api page to retrieve single instance of a given hotel
@app.route('/api/hotels/<int:hotel_id>', methods=['GET'])
def api_hotels_id(hotel_id):
    hotel = Hotel.query.get(hotel_id)
    if not hotel:
        return jsonify({"error": "Hotel not found"}), 404
    
    hotel_data = {
        "name": hotel.name,
        "id": hotel.id,
        "city": hotel.cityName,
        "vicinity": hotel.vicinity,
        "rating": hotel.rating,
        "num_ratings": hotel.total_ratings
    }

    return jsonify(hotel_data)

# api/POIs
# params: none
# api page to retrieve all POI instances fitting given query parameters
@app.route('/api/POIs', methods=['GET'])
def api_pois():
    page_term = int(request.args.get('page', 1))
    per_page_term = int(request.args.get('per_page', 10))
    sort_by_term = request.args.get('sort_by', 'name')
    sort_order_term = request.args.get('sort_order', 'asc')
    city_term = request.args.get('city', 'all')
    min_rating_term = float(request.args.get('min_rating', 0))

    query = PointsOfInterest.query
    if sort_by_term == "name":
        if sort_order_term == "asc":
            if city_term == "all":
                query = query.filter(PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.cityName.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(PointsOfInterest.cityName == city_term and PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.cityName.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
        else:
            if city_term == "all":
                query = query.filter(PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.cityName.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(PointsOfInterest.cityName == city_term and PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.cityName.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
    else:
        if sort_order_term == "asc":
            if city_term == "all":
                query = query.filter(PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.rating.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(PointsOfInterest.cityName == city_term and PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.rating.asc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
        else:
            if city_term == "all":
                query = query.filter(PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.rating.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)
            else:
                query = query.filter(PointsOfInterest.cityName == city_term and PointsOfInterest.rating >= min_rating_term).order_by(PointsOfInterest.rating.desc()).limit(per_page_term).offset((page_term - 1) * per_page_term)

    try:
        pois_list = []
        for poi in query:
            pois_list.append(format_point_of_interest_basic(poi))
        if not pois_list:
            return jsonify({"error": "No results found with given parameters"}), 404
        return jsonify(pois_list)
    except NoResultFound:
        return jsonify({"error": "No results found with given parameters"}), 404

# api/POIs/poi_id
# params: poi_id
# api page to retrieve single instance of a given POI
@app.route('/api/POIs/<int:poi_id>', methods=['GET'])
def api_pois_id(poi_id):
    poi = PointsOfInterest.query.get(poi_id)
    if not poi:
        return jsonify({"error": "Point of interest not found"}), 404
    
    poi_data = {
        "name": poi.name,
        "id": poi.id,
        "city": poi.cityName,
        "vicinity": poi.vicinity,
        "rating": poi.rating,
        "num_ratings": poi.total_ratings
    }

    return jsonify(poi_data)

# api/count/destinations
# params: none
# api page to retrieve total number of destinations
@app.route('/api/count/destinations', methods=['GET'])
def api_count_destinations():
    destinations = Destination.query.all()
    destinations_count = 0
    for destination in destinations:
        destinations_count += 1


    pois = PointsOfInterest.query.all()
    poi_rating_total = 0
    poi_rating_count = 0
    for poi in pois:
        poi_rating_count += 1
        poi_rating_total += poi.rating
    pois_average_rating = poi_rating_total / poi_rating_count

    hotels = Hotel.query.all()
    hotels_rating_total = 0
    hotels_rating_count = 0
    for hotel in hotels:
        hotels_rating_count += 1
        hotels_rating_total += hotel.rating
    hotels_average_rating = hotels_rating_total / hotels_rating_count

    data = {
        "num_destinations": destinations_count,
        "average_POI_rating": pois_average_rating,
        "average_hotel_rating": hotels_average_rating
    }
    
    return jsonify(data)

# api/count/destination_id/hotels
# params: destination_id
# api page to retrieve number of hotels for specified destination
@app.route('/api/count/<string:city_name>/hotels', methods=['GET'])
def api_count_id_hotels(city_name):
    hotels = Hotel.query.filter(Hotel.cityName == city_name).all()
    hotels_rating_total = 0
    hotels_count = 0
    for hotel in hotels:
        hotels_count += 1
        hotels_rating_total += hotel.rating
    hotels_average_rating = hotels_rating_total / hotels_count

    data = {
        "city": city_name,
        "num_hotels": hotels_count,
        "average_hotel_rating": hotels_average_rating
    }
    
    return jsonify(data)

# api/count/destination_id/POIs
# params: destination_id
# api page to retrieve number of POIs for specified destination
@app.route('/api/count/<string:city_name>/POIs', methods=['GET'])
def api_count_id_pois(city_name):
    pois = PointsOfInterest.query.filter(PointsOfInterest.cityName == city_name).all()
    pois_rating_total = 0
    pois_count = 0
    for poi in pois:
        pois_count += 1
        pois_rating_total += poi.rating
    pois_average_rating = pois_rating_total / pois_count
    data = {
        "city": city_name,
        "num_POIs": pois_count,
        "average_POI_rating": pois_average_rating
    }
    
    return jsonify(data)


def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn

def create_destinations_db():
    destinations = load_json('destinations.json')

    for city_info in destinations['destinations']:

        destination = Destination(city_info["cityName"], city_info["location"], city_info["latitude"], city_info["longitude"], city_info["website"], city_info["photo_url"])
        db.session.add(destination)
        db.session.commit()

def create_pointsofinterest_db():
    poi_list = load_json('pointofinterest.json')

    for poi in poi_list['point_of_interest']:
        destination = Destination.query.filter_by(cityName=poi["cityName"]).first()
        point_of_interest = PointsOfInterest(poi["name"], poi["vicinity"], poi["rating"], poi["total_ratings"], poi["photo_url"])
        point_of_interest.destination = destination
        db.session.add(point_of_interest)
        db.session.commit()

def create_hotels_db():
    hotel_list = load_json('hotels.json')

    for hotel in hotel_list['hotels']:
        destination = Destination.query.filter_by(cityName=hotel["cityName"]).first()
        hotel = Hotel(hotel["name"], hotel["vicinity"], hotel["rating"], hotel["total_ratings"], hotel["photo_url"])
        hotel.destination = destination
        db.session.add(hotel)
        db.session.commit()