from flask import Flask, render_template, request
import requests

app = Flask(__name__, template_folder='../frontend')

# API call urls
WIKIPEDIA_API_URL = "https://en.wikipedia.org/w/api.php?action=query&prop=links&titles=List_of_cities_by_international_visitors&format=json"
PLACES_SEARCH_URL = "https://maps.googleapis.com/maps/api/place/findplacefromtext/json"
PLACES_DETAILS_URL = "https://maps.googleapis.com/maps/api/place/details/json"


@app.route('/', methods=['GET'])
def index():
    return ''

@app.route('/destinations', methods=['GET'])
def destinations():
    try:
        api_key = 'AIzaSyBlEBBPgplI48Z3CcjSr9212WzGBHN-uQk'

        # Since URL is static (i.e. no params are ever changed) we just use the string to get the response.
        destinations = requests.get(f"{WIKIPEDIA_API_URL}").json()
        
        # Ensure the destination links are available and within the expected range
        links = destinations["query"]["pages"]["58606989"].get("links", [])
        if len(links) < 3:
            raise Exception("Not enough destination links found.")

        # Get the top 3 cities (Alphabetical order)
        cities = [
            links[0]["title"],
            links[1]["title"],
            links[2]["title"],
        ]
        
        # Empty list that will house the details for each city.
        cityDetails = []

        cityParams = {
            'inputtype': 'textquery',
            'key': api_key,
        }

        cityDetailParams = {
            'key': api_key,
        }
        
        # Loop through the cities and fill out the attributes we want to display on the webpage.
        for cityName in cities:
            cityParams['input'] = cityName
            city = requests.get(PLACES_SEARCH_URL, cityParams).json().get('candidates', [])
            if not city:
                continue
            thisCity = city[0]['place_id']
            cityDetailParams['placeid'] = thisCity
            results = requests.get(PLACES_DETAILS_URL, cityDetailParams).json().get('result', {})
            website = results.get('website', "")
            location = results.get('formatted_address', "")
            lat = results.get('geometry', {}).get('location', {}).get('lat', "")
            long = results.get('geometry', {}).get('location', {}).get('lng', "")
            photo_url = None
            if 'photos' in results:
                photo_reference = results['photos'][0]['photo_reference']
                # In order to access photos, you have to use the API and attach a reference to the photo to get the URL.
                photo_url = f"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_reference}&key={api_key}"
            cityDetails.append({'location': location, 'lat': lat, 'long': long, 'photo_url': photo_url, 'website': website})
        return cityDetails
    except Exception as e:
        return e


@app.route('/hotels', methods=['GET'])
def hotels():
    api_key = 'AIzaSyBlEBBPgplI48Z3CcjSr9212WzGBHN-uQk'
    city_name = 'Austin'

    map_endpoint = "https://maps.googleapis.com/maps/api/geocode/json"
    map_params = {'address': city_name, 'key': api_key}

    map_response = requests.get(map_endpoint, params=map_params)
    map_results = map_response.json().get('results')

    if map_results:
        location = map_results[0]['geometry']['location']
        lat = location['lat']
        lng = location['lng']
        location = f"{lat},{lng}"
        radius = 5000
        keyword='hotel'
        endpoint_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"

        params = {'location': location, 'radius': radius, 'keyword': keyword, 'key': api_key}
        response = requests.get(endpoint_url, params=params, verify=False)
        results = response.json().get('results')

        if results:
            hotel_list = []
            for place in results:
                name = place['name']
                vicinity = place['vicinity']
                rating = place.get('rating', 'Not available')
                user_ratings = place.get('user_ratings_total', 'No ratings')
                photo_url = None
                if 'photos' in place:
                    photo_reference = place['photos'][0]['photo_reference']
                    photo_url = f"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_reference}&key={api_key}"
                hotel_list.append({'name': name, 'vicinity': vicinity, 'rating': rating, 'user_ratings_total': user_ratings, 'photo_url': photo_url})
            return hotel_list
        else:
            return "No hotels found."
    else:
        return f"Could not find coordinates for {city_name}"

@app.route('/pointsofinterest', methods=['GET'])
def pointsofinterest():
    api_key = 'AIzaSyBlEBBPgplI48Z3CcjSr9212WzGBHN-uQk'
    city_name = 'Austin'

    map_endpoint = "https://maps.googleapis.com/maps/api/geocode/json"
    map_params = {'address': city_name, 'key': api_key}

    map_response = requests.get(map_endpoint, params=map_params)
    map_results = map_response.json().get('results')

    if map_results:
        location = map_results[0]['geometry']['location']
        lat = location['lat']
        lng = location['lng']
        location = f"{lat},{lng}"
        radius = 5000
        types='tourist_attraction'
        endpoint_url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json"

        params = {'location': location, 'radius': radius, 'types': types, 'key': api_key}
        response = requests.get(endpoint_url, params=params, verify=False)
        results = response.json().get('results')

        if results:
            poi_list = []
            for place in results:
                name = place['name']
                vicinity = place['vicinity']
                rating = place.get('rating', 'Not available')
                user_ratings = place.get('user_ratings_total', 'No ratings')
                photo_url = None
                if 'photos' in place:
                    photo_reference = place['photos'][0]['photo_reference']
                    photo_url = f"https://maps.googleapis.com/maps/api/place/photo?maxwidth=400&photoreference={photo_reference}&key={api_key}"
                poi_list.append({'name': name, 'vicinity': vicinity, 'rating': rating, 'user_ratings_total': user_ratings, 'photo_url': photo_url})
            return poi_list
        else:
            return "No points of interest found."
    else:
        return f"Could not find coordinates for {city_name}"

if __name__ == '__main__':
    app.run(debug=True)
