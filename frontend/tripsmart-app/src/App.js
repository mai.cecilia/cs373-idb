import React from 'react';
import { Routes, Route, BrowserRouter as Router } from "react-router-dom";
import Home from './components/Home';
import About from './components/About';
import Destinations from './components/Destinations';
import PointsOfInterest from './components/PointsOfInterest'
import Hotels from './components/Hotels';
import OneShot from './components/OneShot';
import Error from './components/Error';
import Api from './components/Api'
import BaseSubroutesApi from './components/BaseSubroutesApi'
import DestinationsApi from './components/DestinationsApi'
import HotelsApi from './components/HotelsApi';
import POIsApi from './components/POIsApi';
import CountDestinationsApi from './components/CountDestinationsApi';
import CountHotelsApi from './components/CountHotelsApi';
import CountPOIsApi from './components/CountPOIsApi';

const App = () => {
    return (
        <Router>
        <div className="App">
            <Routes>
                <Route path="/" element={<Home />} />
                <Route path="/about" element={<About />} />
                <Route path="/destinations" element={<Destinations />}/>
                <Route path="/hotels" element={<Hotels />}/>
                <Route path="/pointsofinterest" element={ <PointsOfInterest />}/>
                <Route path="/oneShot" element={ <OneShot />}/>
                <Route path="/api" element={<Api />} />
                <Route path="/api/count/destinations" element={<CountDestinationsApi />} />
                <Route exact path="/api/:baseSubroute" element={<BaseSubroutesApi />} />
                <Route exact path="/api/destinations/:cityName" element={<DestinationsApi />} />
                <Route exact path="/api/hotels/:id" element={<HotelsApi />} />
                <Route exact path="/api/pois/:id" element={<POIsApi />} />
                <Route exact path="/api/count/:cityName/hotels" element={<CountHotelsApi />} />
                <Route exact path="/api/count/:cityName/POIs" element={<CountPOIsApi />} />
                <Route path="*" element={<Error error="Page not found" />} />
            </Routes>
        </div>
        </Router>
    );
};

export default App;