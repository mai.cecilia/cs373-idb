import React, { useState } from 'react';
import Form from 'react-bootstrap/Form'

const Slider = ({ min, max, step, title, onFilter }) => {
    const [filter, setFilter] = useState(min);

    const handleInputChange = (event) => {
        setFilter(event.target.value);
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (onFilter) {
            console.log(filter)
            onFilter(filter);
        }
    };

    return (
        <div>
            <Form.Label>{title}</Form.Label>
            <Form.Range 
                min={min}
                max={max}
                step={step}
                defaultValue={min}
                onChange={handleInputChange}
                onMouseUp={handleSubmit}
                onTouchEnd={handleSubmit}/>
        </div>
    )
}

export default Slider;