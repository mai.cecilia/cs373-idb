import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';

const OrderingDropdown = ({ onSort }) => {

    const handleAscend = (event) => {
        event.preventDefault();
        if (onSort) {
            onSort(true);
        }
    }

    const handleDescend = (event) => {
        event.preventDefault();
        if (onSort) {
            onSort(false);
        }
    }

    return (
        <>
            <DropdownButton id="dropdown-basic-button" title="Ordering">
                <Dropdown.Item onClick={handleAscend}>Ascending</Dropdown.Item>
                <Dropdown.Item onClick={handleDescend}>Descending</Dropdown.Item>
            </DropdownButton>
        </>
    )
}

export default OrderingDropdown;