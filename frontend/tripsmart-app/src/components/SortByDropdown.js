import Dropdown from 'react-bootstrap/Dropdown';
import DropdownButton from 'react-bootstrap/DropdownButton';
import React from 'react';

const SortByDropdown = ({ onSet, categories }) => {

    const handleSetSortTerm = (category) => {
        if (onSet) {
            onSet(category);
        }
    }

    return (
        <DropdownButton id="dropdown-basic-button" title="Sort By">
            {categories.map(
                (category) => (
                    <Dropdown.Item onClick={() => handleSetSortTerm(category)} key={category}> { category } </Dropdown.Item>
                )
            )}
        </DropdownButton>
    )
}

export default SortByDropdown;