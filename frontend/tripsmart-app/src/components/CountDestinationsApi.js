import { useState, useEffect } from 'react';

function CountDestinationsApi() {
  const [message, setMessage] = useState('');

  useEffect(() => {
    const fetchData = async () => {
        const response = await fetch(`https://cs373-idb-group4-summer2024.uc.r.appspot.com/api/count/destinations`);
        const jsonData = await response.json();
        setMessage(jsonData);
    };

    fetchData()

  }, []);
  
  return (
    <div>
      <pre>{JSON.stringify(message, null, 2)}</pre>
    </div>
  );
}

export default CountDestinationsApi;