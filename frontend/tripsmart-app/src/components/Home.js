import React from 'react';
import { Link } from 'react-router-dom';
import TSNavbar from './TSNavbar';

import SplashImg from '../images/SplashImg.jpeg'

const Home = () => {
    const backgroundStyle = {
        backgroundImage: `url(${SplashImg})`,
        backgroundSize: 'cover',
        backgroundPosition: 'center',
        minHeight: '100vh',
        paddingTop: '75px',
    };

    return (
        <div>
            <TSNavbar />
            <div className="container-fluid" style={backgroundStyle}>
                <div className="row">
                    <div className="col-md-8 offset-md-2 text-center">
                        <h1 className="display-4 mb-4">Welcome to TripSmart</h1>
                        <p className="lead">
                            Find the best destinations, points of interest, and accommodations for your next trip!
                        </p>
                        <Link to="/destinations" className="btn btn-primary btn-lg mt-3">Explore Destinations</Link>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Home;