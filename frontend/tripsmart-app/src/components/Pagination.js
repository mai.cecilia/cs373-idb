import React, { useEffect, useState } from 'react';

const Pagination = ({ currPage, totalItems, itemsPerPage, onPageChange }) => {
    const [pageNumbers, setPageNumbers] = useState([]);
    const maxShownPages = 7;

    useEffect(() => {
        const totalPages = Math.ceil(totalItems / itemsPerPage);

        let startPage, endPage;
        if (totalPages <= maxShownPages) {
            startPage = 1;
            endPage = totalPages;
        } else {
            if (currPage <= Math.ceil(maxShownPages / 2)) {
                startPage = 1;
                endPage = maxShownPages;
            } else if (currPage + Math.floor(maxShownPages / 2) >= totalPages) {
                startPage = totalPages - maxShownPages + 1;
                endPage = totalPages;
            } else {
                startPage = currPage - Math.floor(maxShownPages / 2);
                endPage = currPage + Math.floor(maxShownPages / 2);
            }
        }

        const numbers = [];
        for (let i = startPage; i <= endPage; i++) {
            numbers.push(i);
        }
        setPageNumbers(numbers);
    }, [currPage, totalItems, itemsPerPage]);

    return (
        <nav>
            <ul className="pagination justify-content-center">
                <li className={`page-item ${currPage === 1 ? 'disabled' : ''}`}>
                    <button className="page-link" onClick={() => onPageChange(currPage - 1)}>Previous</button>
                </li>
                {pageNumbers.map(number => (
                    <li key={number} className={`page-item ${currPage === number ? 'active' : ''}`}>
                        <button className="page-link" onClick={() => onPageChange(number)}>{number}</button>
                    </li>
                ))}
                <li className={`page-item ${currPage === Math.ceil(totalItems / itemsPerPage) ? 'disabled' : ''}`}>
                    <button className="page-link" onClick={() => onPageChange(currPage + 1)}>Next</button>
                </li>
            </ul>
        </nav>
    );
};

export default Pagination;
