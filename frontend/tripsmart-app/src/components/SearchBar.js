import React, { useState } from 'react';

const SearchBar = ({ onSearch }) => {
    const [query, setQuery] = useState('');

    const handleInputChange = (event) => {
        const newQuery = event.target.value;
        setQuery(newQuery);
        if (onSearch) {
            onSearch(newQuery);
        }
    };

    const handleSubmit = (event) => {
        event.preventDefault();
        if (onSearch) {
            onSearch(query);
        }
    };

    return (
        <form onSubmit={handleSubmit} className="d-flex justify-content-center my-4">
            <div className="input-group" style={{ maxWidth: '600px', width: '100%' }}>
                <input
                    type="text"
                    value={query}
                    onChange={handleInputChange}
                    placeholder="Search..."
                    style={{ width: '400px'}}
                    className="form-control"
                />
                <button type="submit" className="btn btn-primary">Search</button>
            </div>
        </form>
    );
};

export default SearchBar;
