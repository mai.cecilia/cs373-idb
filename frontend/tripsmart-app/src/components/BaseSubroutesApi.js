import { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';

function BaseSubroutesApi() {
  const [route, setRoute] = useState('');
  const [message, setMessage] = useState('');
  const navigate = useNavigate()

  useEffect(() => {
    const path = window.location.href;
    const parts = path.split('/');
    const last = parts[parts.length - 1];
    if (last.includes('?')) {
      const querySplit = last.split('?');
      const mainRotue = querySplit[0];

      if (mainRotue === "destinations" || mainRotue === "hotels" || mainRotue === "POIs") {
        setRoute(last);
      } else {
        setRoute("errorRoute");
      }
    } else {
      if (last === "destinations" || last === "hotels" || last === "POIs") {
        setRoute(last);
      } else {
        setRoute("errorRoute");
      }
    }

  }, []);

  useEffect(() => {
    const fetchData = async () => {
        const response = await fetch(`https://cs373-idb-group4-summer2024.uc.r.appspot.com/api/${route}`);
        const jsonData = await response.json();
        setMessage(jsonData);
    };

    if (route !== 'errorRoute') {
        fetchData();
    } else {
        console.log("Route not valid");
        navigate("/404");
    }

  }, [route]);

  return (
    <div>
      <pre>{JSON.stringify(message, null, 2)}</pre>
    </div>
  );
}

export default BaseSubroutesApi;