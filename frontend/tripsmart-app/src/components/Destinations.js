import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import TSNavbar from './TSNavbar';
import Pagination from './Pagination';
import SearchBar from './SearchBar';
import OrderingDropdown from './OrderingDropdown';
import SortByDropdown from './SortByDropdown';
import Slider from './Slider';
import './Destinations.css';

const Destinations = () => {
    const [searchedDests, setSearchedDests] = useState([]);
    const [currPage, setCurrPage] = useState(1);
    const [sort, setSort] = useState(true);
    const [sortBy, setSortBy] = useState("City Name");
    const [query, setQuery] = useState("");
    const [filterLatitude, setFilterLatitude] = useState(-90.0);
    const [filterLongitude, setFilterLongitude] = useState(-180.0);
    const [destPerPage] = useState(21); // Default data per page set to 21

    useEffect(() => {
        axios.get('https://cs373-idb-group4-summer2024.uc.r.appspot.com/destinations')
            .then(response => {
                const searchToSet = response.data.destinations;
                const filteredDestinations = searchToSet
                    .filter(dest => dest.location.toLowerCase().includes(query.toLowerCase()))
                    .filter(dest => dest.latitude >= filterLatitude)
                    .filter(dest => dest.longitude >= filterLongitude);

                let sortedDestinations;
                switch (sortBy) {
                    case "City Name":
                        sortedDestinations = filteredDestinations.sort((a, b) => 
                            sort ? a.cityName.localeCompare(b.cityName) : b.cityName.localeCompare(a.cityName));
                        break;
                    case "Location":
                        sortedDestinations = filteredDestinations.sort((a, b) => 
                            sort ? a.location.localeCompare(b.location) : b.location.localeCompare(a.location));
                        break;
                    case "Latitude":
                        sortedDestinations = filteredDestinations.sort((a, b) => 
                            sort ? b.latitude - a.latitude : a.latitude - b.latitude);
                        break;
                    default:
                        sortedDestinations = filteredDestinations.sort((a, b) =>
                            sort ? b.longitude - a.longitude : a.longitude - b.longitude);
                }

                setSearchedDests(sortedDestinations);
                setCurrPage(1);
            })
            .catch(error => {
                console.error('There was an error fetching the destinations!', error);
            });
    }, [query, sort, sortBy, filterLatitude, filterLongitude]);

    const handleSearch = (query) => {
        setQuery(query);
    };

    const handleSort = (sortBy) => {
        setSortBy(sortBy);
    };

    const handleSetSort = (valueOfSort) => {
        setSort(valueOfSort);
    };

    const filterLatitudeFunc = (latitude) => {
        setFilterLatitude(latitude);
    };

    const filterLongitudeFunc = (longitude) => {
        setFilterLongitude(longitude);
    };

    const highlightText = (text, term) => {
        if (!text || !term) return text;
    
        const escapedTerm = term.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
        const regex = new RegExp(`(${escapedTerm})`, 'gi');

        return text.split(regex).map((part, index) => 
            part.toLowerCase() === term.toLowerCase() ? <mark key={index}>{part}</mark> : part
        );
    };

    const lastDestIndex = currPage * destPerPage;
    const firstDestIndex = lastDestIndex - destPerPage;
    const currDest = searchedDests.slice(firstDestIndex, lastDestIndex);

    const paginate = pageNumber => setCurrPage(pageNumber);

    return (
        <div>
            <TSNavbar />
            <div className="container">
                <h2 className="text-center my-4">Popular Destinations</h2>
                <div className="row justify-content-center align-items-center">
                    <div className="col-auto">
                        <SearchBar onSearch={handleSearch} />
                    </div>
                    <div className="col-auto">
                        <OrderingDropdown onSort={handleSetSort} />
                    </div>
                    <div className="col-auto">
                        <SortByDropdown onSet={handleSort} categories={["City Name", "Location", "Latitude", "Longitude"]} />
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-auto">
                        <Slider min={-90.0} max={90.0} step={0.5} title={"Filter Latitude"} onFilter={filterLatitudeFunc} />
                    </div>
                    <div className="col-auto">
                        <Slider min={-180.0} max={180.0} step={0.5} title={"Filter Longitude"} onFilter={filterLongitudeFunc} />
                    </div>
                </div>
                <p className="text-center">Instances: {searchedDests.length}</p>
                <div className="row row-cols-1 row-cols-md-3 g-4">
                    {currDest.map(city => (
                        <div className="col" key={city.id}>
                            <div className="card h-100">
                                <img src={city.photo_url} className="card-img-top img-fluid" alt="Destination" style={{ height: '250px', objectFit: 'cover' }} />
                                <div className="card-body d-flex flex-column">
                                    <h5 className="card-title text-center">{highlightText(city.location, query)}</h5>
                                    <p className="card-text">{highlightText(city.description, query)}</p>
                                    <div className="mt-auto">
                                        <Link to='/oneShot' state={{ isDestination: true, isHotel: false, element: city }} className="btn btn-primary d-block mx-auto">Explore This Destination</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <div style={{ paddingTop: '30px' }}>
                    <Pagination
                        currPage={currPage}
                        totalItems={searchedDests.length}
                        itemsPerPage={destPerPage}
                        onPageChange={paginate}
                    />
                </div>
            </div>
        </div>
    );
};

export default Destinations;
