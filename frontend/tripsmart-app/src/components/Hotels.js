import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { Link } from 'react-router-dom';
import TSNavbar from './TSNavbar';
import Pagination from './Pagination';
import SearchBar from './SearchBar';
import OrderingDropdown from './OrderingDropdown';
import SortByDropdown from './SortByDropdown';
import Slider from './Slider';
import './Hotels.css';

const Hotels = () => {
    const [searchedHotels, setSearchedHotels] = useState([]);
    const [currPage, setCurrPage] = useState(1);
    const [sort, setSort] = useState(true);
    const [sortBy, setSortBy] = useState("City Name");
    const [query, setQuery] = useState("");
    const [filterRating, setFilterRating] = useState(0);
    const [filterTotalRating, setFilterTotalRating] = useState(0);
    const [hotelsPerPage] = useState(21); // Default data per page set to 21

    useEffect(() => {
        axios.get('https://cs373-idb-group4-summer2024.uc.r.appspot.com/hotels')
            .then(response => {
                const searchToSet = response.data.hotels;
                const filteredHotels = searchToSet
                    .filter(hotel => hotel.name.toLowerCase().includes(query.toLowerCase()))
                    .filter(hotel => hotel.rating >= filterRating)
                    .filter(hotel => hotel.total_ratings >= filterTotalRating);

                let sortedHotels;
                switch (sortBy) {
                    case "City Name":
                        sortedHotels = filteredHotels.sort((a, b) => 
                            sort ? a.cityName.localeCompare(b.cityName) : b.cityName.localeCompare(a.cityName));
                        break;
                    case "Names":
                        sortedHotels = filteredHotels.sort((a, b) => 
                            sort ? a.name.localeCompare(b.name) : b.name.localeCompare(a.name));
                        break;
                    case "Ratings":
                        sortedHotels = filteredHotels.sort((a, b) => 
                            sort ? b.rating - a.rating : a.rating - b.rating);
                        break;
                    default:
                        sortedHotels = filteredHotels.sort((a, b) =>
                            sort ? b.total_ratings - a.total_ratings : a.total_ratings - b.total_ratings);
                }

                setSearchedHotels(sortedHotels);
                setCurrPage(1);
            })
            .catch(error => {
                console.error('There was an error fetching the hotels!', error);
            });
    }, [query, sort, sortBy, filterRating, filterTotalRating]);

    const handleSearch = (query) => {
        setQuery(query);
    };

    const handleSort = (sortBy) => {
        setSortBy(sortBy);
    };

    const handleSetSort = (valueOfSort) => {
        setSort(valueOfSort);
    };

    const filterRatingFunc = (rating) => {
        setFilterRating(rating);
    };

    const filterTotalRatingFunc = (rating) => {
        setFilterTotalRating(rating);
    };

    const highlightText = (text, term) => {
        if (!text || !term) return text;
    
        const escapedTerm = term.replace(/[-/\\^$*+?.()|[\]{}]/g, '\\$&');
        const regex = new RegExp(`(${escapedTerm})`, 'gi');

        return text.split(regex).map((part, index) => 
            part.toLowerCase() === term.toLowerCase() ? <mark key={index}>{part}</mark> : part
        );
    };

    const lastHotelIndex = currPage * hotelsPerPage;
    const firstHotelIndex = lastHotelIndex - hotelsPerPage;
    const currHotels = searchedHotels.slice(firstHotelIndex, lastHotelIndex);

    const paginate = pageNumber => setCurrPage(pageNumber);

    return (
        <div>
            <TSNavbar/>
            <div className="container">
            <h2 className="text-center my-4">Popular Hotels</h2>
                <div className="row justify-content-center align-items-center">
                    <div className="col-auto">
                        <SearchBar onSearch={handleSearch} />
                    </div>
                    <div className="col-auto">
                        <OrderingDropdown onSort={handleSetSort} />
                    </div>
                    <div className="col-auto">
                        <SortByDropdown onSet={handleSort} categories={["City Name", "Names", "Ratings", "Total Ratings"]} />
                    </div>
                </div>
                <div className="row justify-content-center">
                    <div className="col-auto">
                        <Slider min={0} max={5} step={0.1} title={"Filter Rating"} onFilter={filterRatingFunc} />
                    </div>
                    <div className="col-auto">
                        <Slider min={0} max={20000} step={100} title={"Filter Total Rating"} onFilter={filterTotalRatingFunc} />
                    </div>
                </div>
                <p className="text-center">Instances: {searchedHotels.length}</p>
                <div className="row row-cols-1 row-cols-md-3 g-4">
                    {currHotels.map(hotel => (
                        <div className="col" key={hotel.id}>
                            <div className="card h-100">
                                <img src={hotel.photo_url} className="card-img-top img-fluid" alt="Hotel" style={{ height: '250px', objectFit: 'cover' }} />
                                <div className="card-body d-flex flex-column">
                                    <h5 className="card-title text-center">{highlightText(hotel.name, query)}</h5>
                                    <p className="card-text">{highlightText(hotel.description, query)}</p>
                                    <div className="mt-auto">
                                        <Link to='/oneShot' state={{ isDestination: false, isHotel: true, element: hotel }} className="btn btn-primary d-block mx-auto">Explore This Hotel</Link>
                                    </div>
                                </div>
                            </div>
                        </div>
                    ))}
                </div>
                <div style={{ paddingTop: '30px' }}>
                    <Pagination
                        currPage={currPage}
                        totalItems={searchedHotels.length}
                        itemsPerPage={hotelsPerPage}
                        onPageChange={paginate}
                    />
                </div>
            </div>
        </div>
    );
};

export default Hotels;
