import { useState, useEffect } from 'react';

function POIsApi() {
  const [id, setID] = useState();
  const [message, setMessage] = useState('');

  useEffect(() => {
    const path = window.location.pathname;
    const parts = path.split('/');
    const last = parts[parts.length - 1];
    setID(last);
  }, []);

    console.log(id);

  useEffect(() => {
    const fetchData = async () => {
        const response = await fetch(`https://cs373-idb-group4-summer2024.uc.r.appspot.com/api/POIs/${id}`);
        const jsonData = await response.json();
        setMessage(jsonData);
    };

    if (id !== '') {
        fetchData();
    }

  }, [id]);

  return (
    <div>
      <pre>{JSON.stringify(message, null, 2)}</pre>
    </div>
  );
}

export default POIsApi;