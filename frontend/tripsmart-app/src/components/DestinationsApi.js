import { useState, useEffect } from 'react';

function DestinationsApi() {
  const [cityName, setCityName] = useState('');
  const [message, setMessage] = useState('');

  useEffect(() => {
    const path = window.location.pathname;
    const parts = path.split('/');
    const last = parts[parts.length - 1];
    setCityName(last);
  }, []);

  useEffect(() => {
    const fetchData = async () => {
        const response = await fetch(`https://cs373-idb-group4-summer2024.uc.r.appspot.com/api/destinations/${cityName}`);
        const jsonData = await response.json();
        setMessage(jsonData);
    };

    if (cityName !== '') {
        fetchData();
    }

  }, [cityName]);

  return (
    <div>
      <pre>{JSON.stringify(message, null, 2)}</pre>
    </div>
  );
}

export default DestinationsApi;