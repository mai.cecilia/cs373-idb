import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { NavLink } from 'react-router-dom';

const TSNavbar = () => {
    return (
        <Navbar bg="light" data-bs-theme="light" expand="lg" style={{ padding: '20px' }}>
            <Container>
                <Navbar.Brand href="/">TripSmart</Navbar.Brand>
                <Navbar.Toggle aria-controls="navbarSupportedContent" />
                <Navbar.Collapse id="navbarSupportedContent">
                    <Nav className="mr-auto" style={{ fontSize: '18px' }}>
                        <Nav.Item>
                            <NavLink to="/destinations" className="nav-link" activeClassName="active">Destinations</NavLink>
                        </Nav.Item>
                        <Nav.Item>
                            <NavLink to="/hotels" className="nav-link" activeClassName="active">Hotels</NavLink>
                        </Nav.Item>
                        <Nav.Item>
                            <NavLink to="/pointsofinterest" className="nav-link" activeClassName="active">Points Of Interest</NavLink>
                        </Nav.Item>
                        <Nav.Item>
                            <NavLink to="/about" className="nav-link" activeClassName="active">About</NavLink>
                        </Nav.Item>
                    </Nav>
                </Navbar.Collapse>
            </Container>
        </Navbar>
    );
}

export default TSNavbar;
