import React, { useEffect, useState } from 'react';
import axios from 'axios';
import TSNavbar from './TSNavbar';

import CeciliaMai from '../images/CeciliaMai.jpg';
import AyonDas from '../images/AyonDas.jpg';
import EliZunker from '../images/EliZunker.jpeg';
import JeffreyChen from '../images/JeffreyChen.jpg';
import NathanielRuiz from '../images/NathanielRuiz.jpg';

import Bootstrap from '../images/Bootstrap.png';
import Discord from '../images/Discord.jpg';
import Flask from '../images/Flask.png';
import GCP from '../images/GCP.png';
import Gitlab from '../images/Gitlab.webp';
import NPM from '../images/npm.png';
import Postman from '../images/Postman.jpg';
import ReactPic from '../images/ReactPic.png';
import VSCode from '../images/VSCode.jpg';

const POSTMAN_COLLECTION_LINK = "https://documenter.getpostman.com/view/36647904/2sA3dyhqdG";
const GITLAB_API_LINK = "https://docs.gitlab.com/ee/api/rest/";
const WIKIPEDIA_API_LINK = "https://www.mediawiki.org/wiki/API:Main_page";
const GOOGLE_MAPS_PLACES_API_LINK = "https://developers.google.com/maps/documentation/places/web-service";

const About = () => {
  const [error, setError] = useState(null);
  const [commitCount, setCommitCount] = useState(null);
  const [issueCount, setIssueCount] = useState(null);

  const projectId = '59138519';
  const API_TOKEN = process.env.cs373_GITLAB_API_TOKEN;

  useEffect(() => {
    const fetchCommits = async () => {
      try {
        const response = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/repository/contributors`, {
          headers: { 'PRIVATE-TOKEN': API_TOKEN }
        });
        setCommitCount(response.data);
      } catch (error) {
        console.error('Error fetching contribution data:', error);
      }
    };

    const fetchIssues = async () => {
      try {
        const response = await axios.get(`https://gitlab.com/api/v4/projects/${projectId}/issues?page=1&per_page=100&state=closed`, {
          headers: { 'PRIVATE-TOKEN': API_TOKEN }
        });
        setIssueCount(response.data);
      } catch (error) {
        console.error('Error fetching issue data:', error);
      }
    };

    fetchCommits();
    fetchIssues();
  }, [projectId, API_TOKEN]);

  const commitCountEmail = (email) => {
    if (!commitCount) {
      return 0;
    }

    const totalCommits = commitCount
      .filter(contributor => contributor.email === email)
      .reduce((acc, contributor) => acc + contributor.commits, 0);

    return totalCommits;
  };

  const issueCountUser = (username) => {
    if (!issueCount) {
      return 0;
    }

    const totalIssues = issueCount
      .filter(issue => issue.closed_by.username === username)
      .reduce((acc, issue) => acc + 1, 0);

    return totalIssues;
  };

  const teamMembers = [
    {
      name: "Ayon Das",
      gitlab_id: "ayondas",
      role: "Backend",
      commits: commitCountEmail("ayon.s.das@hotmail.com"),
      issues: issueCountUser("ayondas"),
      unitTests: 3,
      image: AyonDas,
      description: "I am a dedicated and accomplished computer science student at the University of Texas at Austin, pursuing a Bachelor's of Science in Computer Science as a Turing Scholar with a minor in Economics."
    },
    {
      name: "Cecilia Mai",
      gitlab_id: "mai.cecilia",
      role: "Frontend",
      commits: commitCountEmail("mai.cecilia@utexas.edu"),
      issues: issueCountUser("mai.cecilia"),
      unitTests: 0,
      image: CeciliaMai,
      description: "I'm senior in computer science at UT Austin. I like to spend time with friends and play volleyball with my sister!"
    },
    {
      name: "Eli Zunker",
      role: "Frontend",
      gitlab_id: "ezunker",
      commits: commitCountEmail("epzunker@gmail.com"),
      issues: issueCountUser("ezunker"),
      unitTests: 0,
      image: EliZunker,
      description: "I'm a CS Student at UT Austin graduating at the end of the summer. I'm from Round Rock, TX, and love everything Texas!"
    },
    {
      name: "Jeffrey Chen",
      role: "Backend",
      gitlab_id: "jeffreychen287",
      commits: commitCountEmail("jeffreychen287@gmail.com"),
      issues: issueCountUser("jeffreychen287"),
      unitTests: 0,
      image: JeffreyChen,
      description: "I am a computer science student at the University of Texas at Austin. Currently, I am taking two computer science courses, a software engineering class, and a mobile iOS computing class."
    },
    {
      name: "Nathaniel Ruiz",
      gitlab_id: "Nathan9985",
      role: "Backend",
      commits: commitCountEmail("ruizn9985@gmail.com") + commitCountEmail("ruizn9985@utexas.edu"),
      issues: issueCountUser("Nathan9985"),
      unitTests: 3,
      image: NathanielRuiz,
      description: "Hi! I study computer science at UT Austin, I'm from Brownsville, Texas, and I love obscure fantasy lore."
    }
  ];

  const tools = [
    { name: "NPM", description: "Node Package Manager", image: NPM },
    { name: "Postman", description: "API Development Environment", image: Postman },
    { name: "React", description: "JavaScript Library for Building User Interfaces", image: ReactPic },
    { name: "Flask", description: "Python Web Framework", image: Flask },
    { name: "GCP", description: "Google Cloud Platform", image: GCP },
    { name: "Discord", description: "Communication Platform", image: Discord },
    { name: "Bootstrap", description: "CSS Framework", image: Bootstrap },
    { name: "GitLab", description: "Git Repository Manager", image: Gitlab },
    { name: "VSCode", description: "Visual Studio Code Editor", image: VSCode }
  ];

  return (
    <div>
      <TSNavbar />
      <div className="container mt-5">
        <div className="row">
          <div className="col">
            <h2>About Us</h2>
            <p>
              TripSmart aims to streamline the travel planning process by providing users with a one-stop platform 
              for discovering destinations, finding accommodations, and exploring points of interest worldwide. Our 
              goal is to empower travelers with comprehensive information and intuitive tools to create personalized 
              itineraries tailored to their preferences. Whether you're a solo adventurer or planning a family 
              vacation, TripSmart equips you with essential travel insights, local recommendations, and 
              community-driven tips to ensure seamless and memorable journeys. Start planning your next adventure with 
              TripSmart and embark on a journey filled with discovery and exploration.
            </p>
          </div>
        </div>
        <div className="row mt-5">
          <div className="col-12">
            <h3>Our Team</h3>
          </div>
        </div>
        {error && (
          <div className="alert alert-danger" role="alert">
            {error}
          </div>
        )}
        <div className="row">
          {teamMembers.map((member, index) => (
            <div key={index} className="card" style={{ maxWidth: '250px', marginRight: '10px', marginBottom: '10px' }}>
              <div style={{ paddingTop: '13px' }}>
                <img src={member.image} style={{ width: '100%' }} alt={`${member.name}`} />
              </div>
              <div className="card-body" style={{ display: 'flex', flexDirection: 'column', flexGrow: 1 }}>
                <h5 className="card-title">{member.name}</h5>
                <h6 className="card-subtitle mb-2 text-muted">Role: {member.role}</h6>
                <div style={{ flexGrow: 1 }}>
                  <p>{member.description}</p>
                </div>
                <ul className="card-details" style={{  marginTop: '15px', listStyleType: 'none', padding: 0 }}>
                  <li>GitLab ID: {member.gitlab_id}</li>
                  <li>Issues: {member.issues}</li>
                  <li>Commits: {member.commits}</li>
                  <li>Unit Tests: {member.unitTests}</li>
                </ul>
              </div>
            </div>
          ))}
        </div>
        <center style={{marginTop: '20px'}}>
          <p>
            Total Commits: {commitCount ? commitCount.reduce((acc, contributor) => acc + contributor.commits, 0) : 0}<br/>
            Total Issues: {issueCount ? issueCount.length : 0}<br/>
            Total Unit Tests: 6<br/>
            <a href="https://cs373-tripsmart.postman.co/workspace/CS373~df2fe800-f080-487c-b009-7042fca85077/collection/36647904-e723979a-338c-4d92-8c7a-c4b1466e89ce?action=share&creator=36647904" target="_blank" rel="noopener noreferrer">
              Postman API
            </a><br/>
            <a href="https://gitlab.com/mai.cecilia/cs373-idb/-/issues" target="_blank" rel="noopener noreferrer">
              Gitlab Issue Tracker
            </a><br/>
            <a href="https://gitlab.com/mai.cecilia/cs373-idb" target="_blank" rel="noopener noreferrer">
              Gitlab Repo
            </a><br/>
            <a href="https://gitlab.com/mai.cecilia/cs373-idb/-/wikis/Phase-III:-Technical-Report" target="_blank" rel="noopener noreferrer">
              Gitlab Wiki
            </a><br/>
            <a href="https://speakerdeck.com/cm192/group-4-ibd-presentation" target="_blank" rel="noopener noreferrer">
              Presentation
            </a>
          </p>
        </center>
        <div className="row mt-5">
          <div className="col-12">
            <h3>Tools</h3>
          </div>
        </div>
      <div className="row">
        {tools.map((tool, index) => (
          <div key={index} className="card" style={{ maxWidth: '250px', marginRight: '10px', marginBottom: '10px' }}>
            <div style={{ paddingTop: '13px' }}>
              <img src={tool.image} alt={`${tool.name}`} style={{ width: '100%', height: '200px', objectFit: 'cover' }} />
            </div>
            <div className="card-body">
              <h5 className="card-title">{tool.name}</h5>
              <p className="card-text">{tool.description}</p>
            </div>
          </div>
          ))}
        </div>
        <div className="row mt-5">
          <div className="col text-center mb-2">
            <h3>Data Sources</h3>
          </div>
        </div>
        <div className="row">
          <div className="col md-3"></div>
          <div className="col text-end mb-4">
            <a class="btn btn-primary btn-lg" href={GITLAB_API_LINK} role="button">
              Gitlab API
            </a>
          </div>
          <div className="col text-center mb-4">
            <a class="btn btn-primary btn-lg" href={GOOGLE_MAPS_PLACES_API_LINK} role="button">
              Google Places API
            </a>
          </div>
          <div className="col text-start mb-4">
            <a class="btn btn-primary btn-lg" href={WIKIPEDIA_API_LINK} role="button">
              Wikipedia API
            </a>
          </div>
          <div className="col md-3"></div>
          <div class="w-100"></div>
          <div className="col text-center mb-4">
            <a class="btn btn-primary btn-lg" href={POSTMAN_COLLECTION_LINK} role="button">
              Postman Documentation
            </a>
          </div>
        </div>
      </div>
    </div>
  );
}

export default About;
