import { useState, useEffect } from 'react';
import axios from 'axios';

function Api() {
  const [message, setMessage] = useState('');

  useEffect(() => {
    axios.get('https://cs373-idb-group4-summer2024.uc.r.appspot.com/api')
      .then(response => setMessage(response.data))
  }, []);

  return (
    <div>
      <pre>{JSON.stringify(message.text, null, 2)}</pre>
    </div>
  );
}

export default Api;