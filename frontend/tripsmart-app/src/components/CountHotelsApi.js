import { useState, useEffect } from 'react';

function CountHotelsApi() {
  const [cityName, setCityName] = useState('');
  const [message, setMessage] = useState('');

  useEffect(() => {
    const path = window.location.pathname;
    const parts = path.split('/');
    const destination = parts[parts.length - 2];
    setCityName(destination);
  }, []);

  useEffect(() => {
    const fetchData = async () => {
        const response = await fetch(`https://cs373-idb-group4-summer2024.uc.r.appspot.com/api/count/${cityName}/hotels`);
        const jsonData = await response.json();
        setMessage(jsonData);
    };

    if (cityName !== '') {
        fetchData();
    }

  }, [cityName]);

  return (
    <div>
      <pre>{JSON.stringify(message, null, 2)}</pre>
    </div>
  );
}

export default CountHotelsApi;