import React, { useEffect, useState } from 'react';
import axios from 'axios';
import { useLocation } from 'react-router-dom';
import { Link } from 'react-router-dom';
import TSNavbar from './TSNavbar';

const OneShot = () => {
    const location = useLocation();
    const { isDestination, isHotel, element } = location.state;
    const [ json, setJson ] = useState(element);

    useEffect(() => {
        console.log(location.state)
        var url = "";
        if (isDestination) {
            // url = "/destinations/" + element?.cityName
            url = "https://cs373-idb-group4-summer2024.uc.r.appspot.com/destinations/" + element?.cityName
        } else if (isHotel) {
            // url = "/hotels/" + element?.id
            url = "https://cs373-idb-group4-summer2024.uc.r.appspot.com/hotels/" + element?.id
        } else {
            // url = "/pointsofinterest/" + element?.id
            url = "https://cs373-idb-group4-summer2024.uc.r.appspot.com/pointsofinterest/" + element?.id
        }
        axios.get(url)
            .then(response => {
                setJson(response.data);
            })
            .catch(error => {
                console.error('There was an error fetching the destinations!', error);
            });
    }, [element]);

    function getInfo(element) {
        console.log(element)
        if (isDestination) {
            return (
                <div className="card-body text-center">
                    <h3 className="card-title">{element?.location}</h3>
                    <img src={element?.photo_url} alt="Photo" style={{ maxWidth: '100%' }} />
                    <p className="card-text">
                        Latitude: {element?.latitude} <br /> 
                        Longitude: {element?.longitude} <br />
                        <a href="/hotels">Hotels:</a>
                        {element?.hotels && element?.hotels.map((hotel, index) => (
                            <Link to='/oneShot' key={index} state={{ isDestination: false, isHotel: true, element: hotel }} style={{ marginLeft: 5 }}>
                                {hotel.name}
                            </Link>
                        ))}
                        <br />
                        <a href="/pointsofinterest">Points of Interest:</a>
                        {element?.points_of_interest && element?.points_of_interest.map((poi, index) => (
                            <Link to='/oneShot' key={index} state={{ isDestination: false, isHotel: false, element: poi }} style={{ marginLeft: 5 }}>
                                {poi.name}
                            </Link>
                        ))}
                        <br />
                        {element?.website && <a href={element?.website} className="btn btn-primary mx-2">Explore External Website</a>}
                    </p>
                </div>
            );
        } else if (isHotel) {
            return (
                <div className="card-body text-center">
                    <h3 className="card-title">{element.name}</h3>
                    <img src={element.photo_url} alt="Photo" style={{ maxWidth: '100%' }} />
                    <p>
                        Vicinity: {element.vicinity}<br />
                        Rating: {element.rating}⭐<br />
                        Total Ratings: {element.total_ratings}<br />
                        <a href="/pointsofinterest">Points of Interest:</a>
                        {element.points_of_interest && element.points_of_interest.map((poi, index) => (
                            <Link to='/oneShot' key={index} state={{ isDestination: false, isHotel: false, element: poi }} style={{ marginLeft: 5 }}>
                                {poi.name}
                            </Link>
                        ))}
                        <br />
                    </p>
                    <Link to='/oneShot' state={{ isDestination: true, isHotel: false, element: { ...element } }} className="btn btn-primary mx-2">
                        Go to {element.cityName}
                    </Link>
                </div>
            );
        } else {
            return (
                <div className="card-body text-center">
                    <h3 className="card-title">{element.name}</h3>
                    <img src={element.photo_url} alt="Photo" style={{ maxWidth: '100%' }} />
                    <p>
                        Vicinity: {element.vicinity}<br />
                        Rating: {element.rating}⭐<br />
                        Total Ratings: {element.total_ratings}<br />
                        <a href="/hotels">Hotels:</a>
                        {element.hotels && element.hotels.map((hotel, index) => (
                            <Link to='/oneShot' key={index} state={{ isDestination: false, isHotel: true, element: hotel }} style={{ marginLeft: 5 }}>
                                {hotel.name}
                            </Link>
                        ))}
                        <br />
                    </p>
                    <Link to='/oneShot' state={{ isDestination: true, isHotel: false, element: { ...element } }} className="btn btn-primary mx-2">
                        Go to {element.cityName}
                    </Link>
                </div>
            );
        }
    }

    return (
        <div>
            <TSNavbar />
            <div className="card mb-3" key={element?.id} style={{ maxWidth: '100%' }}>
                { getInfo(json) }
            </div>
        </div>
    );
}

export default OneShot;

